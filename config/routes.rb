Rails.application.routes.draw do
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  devise_for :users
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  root to: 'high_voltage/pages#show', id: 'home'

  Rails.application.routes.draw do
    # Api definition
    namespace :api, defaults: { format: :json }  do
      resources :users, only: [:show]
    end
  end
end

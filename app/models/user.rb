class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable, :recoverable, :rememberable, :validatable

  # History
  has_paper_trail

  # Joins
  has_and_belongs_to_many :roles

  # Methods
  [:admin, :moderator, :staff, :basic].each do |role|
    define_method("#{role}?") { roles.exists?(name: role) }
  end
end
